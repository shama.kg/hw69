<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Comments\CommentStoreRequest;
use App\Http\Requests\Comments\CommentUpdateRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class CommentsController extends ApiV1Controller
{

    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $comments = Comment::all();
        return  CommentResource::collection($comments);
    }


    /**
     * @param CommentStoreRequest $request
     * @return CommentResource
     */
    public function store(CommentStoreRequest $request): CommentResource
    {
        $comment = Comment::create($request->validated());
        return new CommentResource($comment);
    }


    /**
     * @param string $id
     * @return CommentResource|JsonResponse
     */
    public function show(string $id): CommentResource|JsonResponse
    {
        $comment = Comment::find($id);
        if ($comment){
            return new CommentResource($comment);
        }
        return $this->notFound();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CommentUpdateRequest $request, string $id)
    {
        $comment = Comment::find($id);
        $validated = $request->validated();
        if($comment){
            $comment->update($validated);
            return new CommentResource($comment);
        }
        return  $this->notFound();

    }


    /**
     * @param Comment $comment
     * @return Response
     */
    public function destroy(Comment $comment): Response
    {
        $comment->delete();
        return response('', 204);
    }
}
