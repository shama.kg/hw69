<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Articles\ArticalEditRequest;
use App\Http\Requests\Articles\ArticleStoreRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ArticlesController extends ApiV1Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $articles = Article::paginate(8);
        return ArticleResource::collection($articles);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ArticleStoreRequest $request)
    {
        $article = Article::create($request->validated());
        return new ArticleResource($article);
    }


    /**
     * @param string $id
     * @return ArticleResource|JsonResponse
     */
    public function show(string $id): ArticleResource|JsonResponse
    {
        $article = Article::find($id);
        if ($article) {
            $article->with('user');
            $article->makeVisible(['created_at', 'updated_at']);
            return new ArticleResource($article);
        }
        return $this->notFound();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ArticalEditRequest $request, string $id)
    {
        $article = Article::find($id);
        $article->update($request->validated());
        return new ArticleResource($article);
    }


    /**
     * @param string $id
     * @return JsonResponse
     */
    public function destroy(string $id): JsonResponse
    {
        $artical = Article::find($id);
        $artical->delete();
        return response()->json([
            'data' => null,
            'message' => ""
        ], 204);
    }
}
