<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class PersonalAccessTokenController extends ApiV1Controller
{

    public function store(Request $request)
    {
        $request->validate([
           'email' => 'required|email',
           'password' => 'required',
           'device_name' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if(! $user || ! Hash::check($request->password, $user->password)){
            throw ValidationException::withMessages([
               'email' => ['The provided credential are incorrect.']
            ]);
        }
        return ['token' => $user->createToken($request->device_name)->plainTextToken];
    }
}
