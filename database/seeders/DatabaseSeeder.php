<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $users = \App\Models\User::factory()->count(10)->create();
        foreach ($users as $user) {
            \App\Models\Article::factory()->count(rand(3, 5))->has(
                \App\Models\Comment::factory()->for($users->random())
            )->for($user)->create();
        }
    }
}
