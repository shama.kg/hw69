<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ArticlesTest extends TestCase
{
    use DatabaseMigrations;

    private $article;
    private $user;

    private string $url;

    public const DATA = [
        'title' => 'test',
        'body' => 'body test',
        'user_id' => null
    ];

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->article = Article::factory()->for($this->user)->create();
        $this->url = route('articles.store');
    }

    /**
     * @group articles
     */
    public function test_success_create_article(): void
    {
        Sanctum::actingAs(
        $this->user,
        ['view-tasks']
    );
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(201);
        $response->assertJsonStructure(
            [
                'data' => [
                    'title', 'body', 'id', 'user'
                ]
            ]
        );

        $this->assertDatabaseHas('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_title(): void
    {
        Sanctum::actingAs(
            $this->user,
            ['view-tasks']
        );
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['title'] = null;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'title'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_body(): void
    {
        Sanctum::actingAs(
            $this->user,
            ['view-tasks']
        );
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['body'] = null;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'body'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_user_id(): void
    {
        Sanctum::actingAs(
            $this->user,
            ['view-tasks']
        );
        $data = self::DATA;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'user_id'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_all_data(): void
    {
        Sanctum::actingAs(
            $this->user,
            ['view-tasks']
        );
        $data = [];
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'title',
                    'body',
                    'user_id'
                ]
            ]
        );
    }

    /**
     * @group articles
     */
    public function test_index_articles()
    {
            Article::factory()->count(10)->for($this->user)->create();
            $response = $this->getJson('api/v1/articles');
            $response->assertStatus(200);
            $response->assertJsonStructure([
                'data' =>[
                    '*' => ['title', 'body', 'user', 'id'],
                ]
            ]);
    }


    /**
     * @group articles
     */
    public function test_success_update_article(): void
    {
        Sanctum::actingAs(
            $this->user,
            ['view-tasks']
        );
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->putJson(route('articles.update', ['article' => $this->article]), $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data' => [
                    'title', 'body', 'id', 'user'
                ]
            ]
        );

        $this->assertDatabaseHas('articles', $data);
    }



    /**
     * @group articles
     */
    public function test_trying_update_article_without_user_id(): void
    {
        Sanctum::actingAs(
            $this->user,
            ['view-tasks']
        );
        $data = self::DATA;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->putJson(route('articles.update', ['article' => $this->article]), $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'user_id'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }


    /**
     * @group articles
     */
    public function test_destroy_article_success()
    {
        Sanctum::actingAs(
            $this->user,
            ['view-tasks']
        );
        $this->assertModelExists($this->article);
        $response = $this->deleteJson(route('articles.destroy',['article' => $this->article]));
        $response->assertStatus(204);
        $response->assertNoContent();
        $this->assertModelMissing($this->article);
    }


    /**
     * @group articles
     */
    public function test_trying_no_auth_comment_delete()
    {

        $this->assertModelExists($this->article);
        $response = $this->deleteJson(route('comments.destroy',['comment'=> $this->article]));
        $response->assertStatus(401);
        $response->assertJsonPath('message', "Unauthenticated.");
        $this->assertModelExists($this->article);

    }



}
