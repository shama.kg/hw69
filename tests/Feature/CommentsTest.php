<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class CommentsTest extends TestCase
{
    use DatabaseMigrations;

    private $user;
    private $article;
    private $url;
    private $comment;

    public const DATA = [
        'body' => 'body test',
        'user_id' => null,
        'article_id' => null,
    ];

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->article = Article::factory()->for($this->user)->create();
        $this->comment = Comment::factory()->for($this->user)->for($this->article)->create();
        $this->url = route('comments.store');
    }

    /**
     * @group comment
     */
    public function test_comments_index(): void
    {
        $response = $this->getJson('api/v1/comments');
    $response->dump();
        $response->assertOk();
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data' => [
                          '*' =>   ['id', 'body', 'user', 'article']
                ]
            ]
        );


    }

    /**
     * @group comment
     */
    public function test_comments_show(): void
    {
        Sanctum::actingAs(
            $this->user,
            ['view-tasks']
        );

        $comment = Comment::factory()->for(Article::factory()->for($this->user)->create())->create();
        $response = $this->get('api/v1/comments/'.  $comment->id);
        $response->assertOk();
        $response->assertJsonStructure([
                'data' => [
                    'body', 'id', 'user', 'article'
                ]
            ]);
        $response->assertJsonPath('data.user.id',  $this->user->name . ' - ' . $this->user->email);

    }


    /**
     * @group comment
     */
    public function test_store_comment()
    {
        Sanctum::actingAs(
            $this->user,
            ['view-tasks']
        );

        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['article_id'] = $this->article->id;
        $this->assertDatabaseMissing('comments', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(201);
        $response->assertJsonStructure(
            [
                'data' => [
                    'article', 'body', 'id', 'user'
                ]
            ]
        );

        $this->assertDatabaseHas('comments', $data);
        $this->assertAuthenticated();
    }


    /**
     * @group comment
     */
    public function test_trying_no_auth_store_comment()
    {
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['article_id'] = $this->article->id;
        $this->assertDatabaseMissing('comments', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(401);
        $response->assertJsonPath(
                'message', "Unauthenticated.",
        );

        $this->assertDatabaseMissing('comments', $data);
        $this->assertGuest();

    }

    /**
     * @group comment
     */
    public function test_update_comment_success()
    {
        Sanctum::actingAs(
            $this->user,
            ['view-tasks']
        );

        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['article_id'] = $this->article->id;
        $this->assertDatabaseMissing('comments', $data);
        $response = $this->putJson(route('comments.update',['comment' => $this->comment]), $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data' => [
                    'article', 'body', 'id', 'user'
                ]
            ]
        );

        $this->assertDatabaseHas('comments', $data);
        $this->assertAuthenticated();
    }


    /**
     * @group comment
     */
    public function test_trying_with_mistake_body_validate_update_comment()
    {
        Sanctum::actingAs(
            $this->user,
            ['view-tasks']
        );

        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['article_id'] = $this->article->id;
        $data['body'] = '';
        $this->assertDatabaseMissing('comments', $data);
        $response = $this->putJson(route('comments.update',['comment' => $this->comment]), $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'body'
                ]
            ]
        );

        $this->assertDatabaseMissing('comments', $data);
        $this->assertAuthenticated();
    }

    /**
     * @group comment
     */
    public function test_destroy_comment_success()
    {
        Sanctum::actingAs(
            $this->user,
            ['view-tasks']
        );
        $this->assertModelExists($this->comment);
        $this->assertDatabaseHas('comments', [
            'body' => $this->comment->body,
        ]);
        $response = $this->deleteJson(route('comments.destroy',['comment' => $this->comment]));
        $response->assertStatus(204);
        $response->assertNoContent();
        $this->assertModelMissing($this->comment);
    }


    /**
     * @group comment
     */
    public function test_trying_no_auth_comment_delete()
    {

        $this->assertModelExists($this->comment);
        $response = $this->deleteJson(route('comments.destroy',['comment'=> $this->comment]));
        $response->assertStatus(401);
        $response->assertJsonPath('message', "Unauthenticated.");
        $this->assertModelExists($this->comment);

    }


}
